package youtube_downloader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import static youtube_downloader.Converter.convertVideo;
import static youtube_downloader.Main.logger;
import youtube_downloader.utils.FileUtils;

/**
 *
 * @author t7emon
 */
public class PlaylistDownloader {

    private static String videoTitle;
    private static int videoCounter;

 public static void Download() throws FileNotFoundException, IOException {
        /*
         * Add browser driver to system properties
         */
          System.setProperty("webdriver.chrome.driver",
           "libs/chromedriver");
      
     /*
      * Initialize the browser drivers
      */
        WebDriver driver = new ChromeDriver();
        WebDriver driverDL = new ChromeDriver();
       /*
        * Scan urls from the video urls file
        */
        try {
               Scanner scanner = new Scanner(Constants.PLAYLIST_URLS_FILE);
            while (scanner.hasNextLine()) {
                /*
                * Retrieve video url
                */
                String playlistUrl = scanner.nextLine();
                logger.info("Convert playlist : " + playlistUrl);
            driver.get(playlistUrl);
            }
            Thread.sleep(2000);
            
        /*
         * Video selector
         */
            String videosSelector = "a.yt-simple-endpoint.style-scope.ytd-playlist-video-renderer";
              
         /*
          * List playlist video's
          */
            List<WebElement> playlist_videos = driver.findElements(By.cssSelector(videosSelector));
            Iterator<WebElement> video_iterator = playlist_videos.iterator();
          /*
           * List video titles
           */
           List<WebElement> video_title = driver.findElements(By.xpath("//span[@id='video-title']"));
           Iterator<WebElement> title_iterator = video_title.iterator();
         
           /*
            * Iterate
            */
            while(video_iterator.hasNext() && title_iterator.hasNext()) {
                videoCounter++; //count
                WebElement video = video_iterator.next();
                WebElement title = title_iterator.next();
               /*
                * Retrieve video url
                */
                String url = video.getAttribute("href");
          /*
           * Retrieve video title
           */
           videoTitle = title.getAttribute("Title");
           
            /*
            * Print video title
           */
            logger.info("Video_Title = " + videoTitle);
             
              /*
              * Convert the video & download
             */
            convertVideo(driverDL, url, Constants.Extention);
             }
            
            TimeUnit.MINUTES.sleep(Constants.minutes_to_keep_thread_alive);
            //Thread.sleep(86400000); //86400000 = 24 hours?
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
             logger.info("Succesfully downloaded video's.");
             logger.info("Stopping drivers...");
            driver.close();
            driverDL.close();
            driver.quit();
            driverDL.quit();
           /*
            * Move downloaded video's
            */
            if (!Constants.VIDEO_STORAGE_DIRECTORY.exists()) {
                logger.info("Creating video storage directory " + Constants.VIDEO_STORAGE_DIRECTORY.getAbsolutePath());
                Constants.VIDEO_STORAGE_DIRECTORY.mkdir();
            }
            logger.info("Moving video's from " + Constants.DOWNLOADS_DIRECTORY + " to " + Constants.VIDEO_STORAGE_DIRECTORY);
            FileUtils.rename(Constants.DOWNLOADS_DIRECTORY, Constants.VIDEO_STORAGE_DIRECTORY, "mp3", "mp4");
        }
        FileUtils.logDownloads();
 }}
