package youtube_downloader.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import youtube_downloader.Constants;
import static youtube_downloader.Main.logger;

/**
 *
 * @author t7emon
 */
public class FileUtils {
  
    public static boolean exists(File src, File dst) {
        File[] srcFiles = src.listFiles();
         for (int i=0; i<srcFiles.length; i++){
        if (srcFiles[i].getName().equalsIgnoreCase(dst.getName())){       
        }
        
         }return true;
}
 public static void rename(File src, File dst, String extention, String extention2) {
    File[] files = src.listFiles();
    for (int i=0; i<files.length; i++){
        if (files[i].getName().endsWith(extention) || files[i].getName().endsWith(extention2)){
            files[i].renameTo(new File(dst, files[i].getName()));
            logger.info("Succesfully moved : " + files[i].getName() + " to " + dst);
        }
    }}
 
  public static void getFileInfo(File src) {
         File[] files = src.listFiles();
          for (int i=0; i<files.length; i++){
            System.out.println("File name : " + files[i].getName() + " | size : " + files[i].length() + " bytes");
        }
       System.out.println("File count : " + files.length);
  }
  
    public static void logDownloads() throws IOException {
          BufferedWriter Writer = new BufferedWriter(new FileWriter(Constants.VIDEO_STORAGE_DIRECTORY + Constants.SEPARATOR + "Downloads.json"));
          File[] files = Constants.VIDEO_STORAGE_DIRECTORY.listFiles();
          for (int i=0; i<files.length; i++){
              if (files[i].getName().endsWith("mp3") || files[i].getName().endsWith("mp4")){
           System.out.println("File name : " + files[i].getName() + " | size : " + files[i].length() + " bytes");
          JSONObject obj = new JSONObject();
           obj.put(i, files[i].getName() + " " + files[i].length());
          JSONArray array = new JSONArray();
           array.add(obj);
           Writer.append(array.toJSONString() + "\n");
          }}
          System.out.println("file count : " + files.length);
           Writer.flush();
           Writer.close();
    }

    public static void countVideoUrls() {
                try {
                    int count = 0;
            Scanner scanner = new Scanner(Constants.VIDEO_URLS_FILE);
            while (scanner.hasNextLine()) {
                scanner.nextLine();
                count++;
                System.out.println("video url count : " + count);
    }
}       catch (FileNotFoundException ex) {
            Logger.getLogger(FileUtils.class.getName()).log(Level.SEVERE, null, ex);
}}}

