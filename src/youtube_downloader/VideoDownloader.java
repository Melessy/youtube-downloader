package youtube_downloader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import static youtube_downloader.Converter.convertVideo;
import static youtube_downloader.Main.logger;
import youtube_downloader.utils.FileUtils;

/**
 *
 * @author t7emon
 */
public class VideoDownloader {
    
    public static void Download() throws FileNotFoundException {
        /*
         * Add browser driver to System properties
         */
          System.setProperty("webdriver.chrome.driver",
           "libs/chromedriver");
          System.setProperty("webdriver.gecko.driver",
                  "libs/geckodriver");
         
      /*
      * Initialize the browser driver, 
        Only chrome browser supported for this downloader atm
      */
        WebDriver driver = new ChromeDriver();
      //WebDriver driver = new FirefoxDriver();
       
        /*
        * Scan urls from the video urls file
        */
        try {
            Scanner scanner = new Scanner(Constants.VIDEO_URLS_FILE);
            while (scanner.hasNextLine()) {
               /*
                * Retrieve video url
                */
                String url = scanner.nextLine();
                /*
                * Convert video
                */
                logger.info("Convert video : " + url);
                convertVideo(driver, url, Constants.Extention);
                }  
            TimeUnit.MINUTES.sleep(Constants.minutes_to_keep_thread_alive);
            //Thread.sleep(86400000); //86400000 = 24 hour?
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            /*
            * Stop the browser driver
            */
             logger.info("Succesfully downloaded video's.");
             logger.info("Stopping drivers...");
            driver.close();
            driver.quit();
           /*
            * Move downloaded video's
            */
            if (!Constants.VIDEO_STORAGE_DIRECTORY.exists()) {
                logger.info("Creating video storage directory " + Constants.VIDEO_STORAGE_DIRECTORY.getAbsolutePath());
                Constants.VIDEO_STORAGE_DIRECTORY.mkdir();
            }
            logger.info("Moving video's from " + Constants.DOWNLOADS_DIRECTORY + " to " + Constants.VIDEO_STORAGE_DIRECTORY);
            FileUtils.rename(Constants.DOWNLOADS_DIRECTORY, Constants.VIDEO_STORAGE_DIRECTORY, "mp3", "mp4");
        }
        try {
            FileUtils.logDownloads();
        } catch (IOException ex) {
            Logger.getLogger(VideoDownloader.class.getName()).log(Level.SEVERE, null, ex);
        }
 }
}
