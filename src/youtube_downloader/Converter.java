package youtube_downloader;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author t7emon
 */
public class Converter {
    
     public static void convertVideo(WebDriver driver, String url, String extention) {
        String converterSite = "https://ytmp3.cc/en2/";
        driver.get(converterSite);
        
         if (extention.toLowerCase().equalsIgnoreCase("mp4")) {   
        // Click mp4
        WebElement MP4_Button = driver.findElement(By.cssSelector("a#mp4"));
        MP4_Button.click();
         }
         
        // Paste url
        WebElement searchBar = driver.findElement(By.cssSelector("input#input"));
        searchBar.sendKeys(url);

        // Click download
        WebElement convertButton = driver.findElement(By.cssSelector("input#submit"));
        convertButton.click();

        // Wait until finished converting video
        WebDriverWait wait = new WebDriverWait(driver, 5000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Download")));

        // Download
        driver.findElement(By.linkText("Download")).click();
    }
    
      public static void convertVideo_MP3(WebDriver driver, String url) {
        String converterSite = "https://ytmp3.cc/en2/";
        driver.get(converterSite);

        // Paste url
        WebElement searchBar = driver.findElement(By.cssSelector("input#input"));
        searchBar.sendKeys(url);

        // Click download
        WebElement convertButton = driver.findElement(By.cssSelector("input#submit"));
        convertButton.click();

        // Wait until finished converting video
        WebDriverWait wait = new WebDriverWait(driver, 5000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Download")));

        // Download
        driver.findElement(By.linkText("Download")).click();
    }
      
        public static void convertVideo_MP4(WebDriver driver, String url) {
        String converterSite = "https://ytmp3.cc/en2/";
        driver.get(converterSite);
        
        // Click mp4
        WebElement MP4_Button = driver.findElement(By.cssSelector("a#mp4"));
        MP4_Button.click();


        // Paste url
        WebElement searchBar = driver.findElement(By.cssSelector("input#input"));
        searchBar.sendKeys(url);

        // Click download
        WebElement convertButton = driver.findElement(By.cssSelector("input#submit"));
        convertButton.click();

        // Wait until finished converting video
        WebDriverWait wait = new WebDriverWait(driver, 5000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Download")));

        // Download
        driver.findElement(By.linkText("Download")).click();
    }
}
