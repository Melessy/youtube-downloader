package youtube_downloader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Logger;
import youtube_downloader.utils.FileUtils;

/**
 *
 * @author t7emon
 */
public class Main {
    /*
    * Variables
    */
    public static Logger logger;
    
    /*
    * Initialize
    */
         private static void init() throws IOException {
             /*
             * Initialize the logger
             */
            logger = Logger.getLogger(Main.class.getName()); 
            /*
            * Create needed Directories & files
            */
            if (!Constants.MAIN_DIRECTORY.exists()) {
            logger.info("Create main directory : " + Constants.MAIN_DIRECTORY.getAbsolutePath());
            Constants.MAIN_DIRECTORY.mkdir();
            }
            if (!Constants.VIDEO_URLS_FILE.exists()) {
               logger.info("Create video urls file : " + Constants.VIDEO_URLS_FILE.getAbsolutePath());
            Constants.VIDEO_URLS_FILE.createNewFile();
            }
            if (!Constants.PLAYLIST_URLS_FILE.exists()) {
                logger.info("Create playlist urls file : " + Constants.PLAYLIST_URLS_FILE.getAbsolutePath());
            Constants.PLAYLIST_URLS_FILE.createNewFile();
            }
            if (!Constants.VIDEO_DOWNLOADS_DIRECTORY.exists()) {
                logger.info("Create downloads directory : " + Constants.VIDEO_DOWNLOADS_DIRECTORY);
            Constants.VIDEO_DOWNLOADS_DIRECTORY.mkdir();
}}
    /**
     * @param args the command line arguments
     */
 public static void main(String[] args) throws FileNotFoundException, IOException {
       init();   
       FileUtils.getFileInfo(new File("/home/t7emon/Desktop/Youtube_Videos/Downloads/Merged/"));
      //FileUtils.countVideoUrls();
              //PlaylistDownloader.Download();
              //VideoDownloader.Download();
         
               
               
        //File_Utils.rename(Constants.DOWNLOADS_DIRECTORY, Constants.DESTINATION_DIRECTORY, "mp3", "mp4");
         

 }}