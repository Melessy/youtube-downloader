package youtube_downloader;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author t7emon
 */
public class Constants {
    
    /*
    * @Format to download video in.
    * Only MP3 & MP4 are supported.
    */
    
    public static String Extention = "MP3"; //MP3, MP4
    
    /*
    * @Amount of time to keep thread running until every download has finished so it won't break while downloading
    */
    public static int minutes_to_keep_thread_alive = 1;
    
    /*
    * USED TO STORE FILES.
    */
    public static final String USER_HOME = System.getProperty("user.home");
    public static final String SEPARATOR = System.getProperty("file.separator");
    
    public static final File DOWNLOADS_DIRECTORY = new File(USER_HOME, "Downloads");
    public static final File DESKTOP_DIRECTORY = new File(USER_HOME, "Desktop");
    public static final File DOCUMENTS_DIRECTORY = new File(USER_HOME, "Documents");
    public static final File MAIN_DIRECTORY = new File(DESKTOP_DIRECTORY, "Youtube_Videos");
    
    public static final File VIDEO_URLS_FILE = new File(MAIN_DIRECTORY, "youtube_video_urls.txt"); //Downloads from multiple urls
    public static File PLAYLIST_URLS_FILE = new File(MAIN_DIRECTORY, "youtube_playlist_urls.txt"); //Untested with multiple playlist urls
    
    public static final File VIDEO_TITLE_FILE = new File(MAIN_DIRECTORY, "video_list.json");
    
     public static String getCurrentDateString()
	    {
	        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	        return dateFormat.format(new Date());
	    }
         public static String getCurrentDateTimeString(String s)
	    {
              //DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
	        DateFormat dateFormat = new SimpleDateFormat(s);
	        return dateFormat.format(new Date());
	    }
     public static final File VIDEO_DOWNLOADS_DIRECTORY = new File(MAIN_DIRECTORY, "Downloads");
     public static final File VIDEO_STORAGE_DIRECTORY = new File(VIDEO_DOWNLOADS_DIRECTORY, getCurrentDateTimeString("dd-MM-yyyy_HH:mm"));
}
